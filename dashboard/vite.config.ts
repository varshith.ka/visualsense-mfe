import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import federation from '@originjs/vite-plugin-federation'
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(),
    federation({
      name:'dashboard',
      filename:'dashboard-entry.js',
      // all the installed libraries we need to sepecify here
      shared:['react','react-dom'],
      // components which we want to expose to other apps
      exposes:{
        './Dashboard':'./src/App',
    }
    })
  ],
  preview:{
    port:5006,
    open:true
  },
  build:{
    modulePreload:false,
    target:'esnext',
    minify:false,
    cssCodeSplit:false
  }
})
