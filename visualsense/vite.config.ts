import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import federation from '@originjs/vite-plugin-federation'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(),
    federation({
      name: 'app1',
      remotes:{
        // urls of the child components
        dashboardApp:'http://localhost:5006/assets/dashboard-entry.js'
      },
      shared:['react','react-dom','react-router-dom']
  }),
  ],
  preview:{
    port:5005,
    open:true
  },
  build:{
    modulePreload:false,
    target:'esnext',
    minify:false,
    cssCodeSplit:false
  }
})
